#include <stdio.h>
#include<string.h>

#define MAX_STRING_LEN 80

int displayMJHMS(unsigned char nb){
    int mois = nb;

    return mois;
}

int annee(unsigned char nb, unsigned char nb2){
    int annee = (nb2<<8)|nb;
    
    return annee;
}

int checkSum(unsigned char seq){
    return 0;
}

int main(){
    FILE* file = fopen("./16040118.ubx", "rb");
    FILE* csv = fopen("./file.csv", "w+");

    if(!file){
        printf("Error ouverture du fichier.");
        return 1;
    }
    if(!csv){
        printf("Error creation/lecture fichier csv.");
        return 1;
    }

    fprintf(csv, "annee,mois,jour,heure,min,sec\n");

    unsigned char id[4];
    unsigned char payload[24];

    while(fread(&id, sizeof(char), 1, file)){
        if(id[0] == 0xb5){
            fread(&id, sizeof(char), 1, file);
            if(id[0] == 0x62){
                fread(&id, sizeof(char), 1, file);
                if(id[0] == 0x01){
                    fread(&id, sizeof(char), 1, file);
                    if(id[0] == 0x21){
                        fread(&payload, sizeof(char), 20+2+2, file);
                        printf("Nouveau message");
                        //printf(" annee %d mois %d jour %d heure %d minute %d seconde %d\n", annee(payload[14], payload[15]), displayMJHMS(payload[16]), displayMJHMS(payload[17]), displayMJHMS(payload[18]), displayMJHMS(payload[19]), displayMJHMS(payload[20]));
                        fprintf(csv, "%i,%i,%i,%i,%i,%i \n", annee(payload[14], payload[15]), displayMJHMS(payload[16]), displayMJHMS(payload[17]), displayMJHMS(payload[18]), displayMJHMS(payload[19]), displayMJHMS(payload[20]));
                    }
                }
            }
        }
    }

    fclose(file);
    fclose(csv);

    /*
    printf("\ntest seconde : %d\n", displayMJHMS(0x0d));
    printf("test minute : %d\n", displayMJHMS(0x00));
    printf("test heure : %d\n", displayMJHMS(0x12));
    printf("test jour : %d\n", displayMJHMS(0x01));
    printf("test mois : %d\n", displayMJHMS(0x04));
    printf("test annee : %d\n", annee(0xe0, 0x07));*/

    return 0;
}