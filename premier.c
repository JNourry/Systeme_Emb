#include<stdio.h>
#include <stdlib.h>
#include <limits.h>

void operation(int *s, int *so, int *m, int *d, int a, int b){
	*s = a+b;
	*so = a-b;
	*m = a*b;
	*d = a/b;
}

void affichebin(unsigned n)
{
	unsigned bit = 0 ;
	unsigned mask = 1 ;
	for (int i = 0 ; i < 32 ; ++i)
	{
		bit = (n & mask) >> i ;
		printf("%d", bit) ;
		mask <<= 1 ;
	}
}

int main(){
	// ==== SIZE OF
	/*
	long int a = 5;
	long b =6;
	long long c = 2;
	char d = 127;
	float e = 2.0;
	double f = 5.0;

	printf("taille int : %lu \n", sizeof(a));
	printf("taille long : %lu \n", sizeof(b));
	printf("taille long long : %lu \n", sizeof(c));
	printf("taille char : %lu \n", sizeof(d));
	printf("taille float : %lu \n", sizeof(e));
	printf("taille double : %lu \n", sizeof(f));*/

	// ==== LIMIT.H
	/*
	printf("The number of bits in a byte %d\n", CHAR_BIT);
	
	printf("The minimum value of SIGNED CHAR = %d\n", SCHAR_MIN);
	printf("The maximum value of SIGNED CHAR = %d\n", SCHAR_MAX);
	printf("The maximum value of UNSIGNED CHAR = %d\n", UCHAR_MAX);

	printf("The minimum value of SHORT INT = %d\n", SHRT_MIN);
	printf("The maximum value of SHORT INT = %d\n", SHRT_MAX); 

	printf("The minimum value of INT = %d\n", INT_MIN);
	printf("The maximum value of INT = %d\n", INT_MAX);

	printf("The minimum value of CHAR = %d\n", CHAR_MIN);
	printf("The maximum value of CHAR = %d\n", CHAR_MAX);

	printf("The minimum value of LONG = %ld\n", LONG_MIN);
	printf("The maximum value of LONG = %ld\n", LONG_MAX);*/

	// ==== OPERATION ARITHMETIQUE
	/*
	double a = 5./9;
	printf("%g\n", a);*/

	// ==== POINTEURS
	/*
	int a=5;
	int* b= &a;

	*b=6;
	printf("la valeur de la variable : %d\n", a);
	printf("l'adresse de la variable : %p\n", &a);*/

	// ==== MALLOC
	/*
	long unsigned int tailleTot = 0;
	int* b=malloc(8*sizeof(int));
	b[1]=5;

	for (int i=0; i<8; i++){
		tailleTot += sizeof(b[i]);
	}
	printf("taille mémoire allouée b : %lu\n", tailleTot);*/

	// ==== PASSAGE PAR VALEUR / PAR REFERENCE
	/*
	int s, so, m, d;
	operation(&s, &so, &m, &d, 10, 2);

	printf("addition : %d\n", s);
	printf("soustraction : %d\n", so);
	printf("multiplication : %d\n", m);
	printf("division : %d\n", d);*/

	// ==== OPERATEUR BINAIRE

	/*119 = 0111 0111 = 0x77
	60 & 13 = 0011 1100 & 0000 1101 = 0000 1100

	unsigned a = 10;
	affichebin(a);*/

	unsigned char c1 = 3;
	unsigned char c2 = 6;
	unsigned char c3 = 0xeb;
	unsigned char c4 = 0b00111111;

	int result;

	result = c1;
	result = result << 8;
	result = result | c2;
	result = result << 8;
	result = result | c3;
	result = result << 8;
	result = result | c4;

	printf("result = %d\n", result);

	return 0;
}